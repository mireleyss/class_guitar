<?php

    class Guitar {
        private string $type;
        private string $color;
        private string $material;
        private string $sound;


        public function __construct(string $type, string $color, string $material, string $sound)
        {
            $this->type = $type;
            $this->color = $color;
            $this->material = $material;
            $this->sound = $sound;
        }

        public function getType() {
            return $this->type;
        }

        public function getColor() {
            return $this->color;
        }

        public function getInfo() {
            return $this->color . ' гитара из материала ' . $this->material . ', ' . $this->type . ' делает ' . $this->sound . '!'; 
        }

        public function setType($type) {
            return $this->type = $type;
        }
    }

    $guitar = new Guitar('акустическая', 'черная', 'ясень', 'дзынь');
    echo $guitar->getInfo() . '<br><br>';
    echo $guitar->getType()  . '<br><br>';
    $guitar->setType('электро');
    echo $guitar->getType()  . '<br><br>';
    echo $guitar->getColor() . '<br><br>';